package com.psb.merkurev.jwttest;

import junit.framework.Assert;

import org.junit.Test;

import java.util.Map;

import io.jsonwebtoken.Header;
import io.jsonwebtoken.Jwt;
import io.jsonwebtoken.Jwts;

/**
 * Created by merkurev on 14.11.16.
 *
 */

public class JWTTest {
    private String tokenPSB = "eyJhbGciOiJSUzI1NiIsImtpZCI6IkNBMDA2MDcwNUYwQTBCOERDOTcxRURBNjQ2ODUyM0RDQTRGQzJEMDgiLCJ0eXAiOiJKV1QiLCJ4NXQiOiJ5Z0JnY0Y4S0M0M0pjZTJtUm9VajNLVDhMUWcifQ.eyJuYmYiOjE0NzkxMzQ2NjksImV4cCI6MTQ3OTEzNTI2OSwiaXNzIjoiaHR0cHM6Ly9yYnAtY2xpZW50YXBwL2lkZW50aXR5IiwiYXVkIjoiaHR0cHM6Ly9yYnAtY2xpZW50YXBwL2lkZW50aXR5L3Jlc291cmNlcyIsImNsaWVudF9pZCI6InJicENsaWVudE1vYmlsZSIsInN1YiI6IjkiLCJhdXRoX3RpbWUiOjE0NzkxMzQ2NjksImlkcCI6ImxvY2FsIiwiaW5jYnQvc2Vzc2lkIjoiNTE0IiwiaW5jYnQvYXBwaWQiOiIyIiwiaW5jYnQvY2hubGlkIjoiMiIsImluY2J0L3VzcmZ1bGwiOiLQodC80LjRgNC90L7QsiDQlNC20L7QvSDQkNCx0YDQsNC80L7QstC40YciLCJpbmNidC9wcm0iOlsiQWNjb3VudC5BY2NlcHQiLCJBY2NvdW50LlZpZXciXSwiaW5jYnQvY2xudGlkIjoiMTMiLCJzY29wZSI6WyJjbGllbnRhcGkiLCJpZGVudGl0eSIsIm9mZmxpbmVfYWNjZXNzIl0sImFtciI6WyJyYnBfc2VjcmV0Y29kZSJdfQ.SzQE5rJKYBsPbifTClrFG9T6JCs1oW77_sLfHfsm0dibUXH8ie0iG-5sgxygfYJhvmJepAPyd2CIWuvIyOrIhXpkn48X7lt6rF9QVpeFjAPOYPdMpuQDo2Uy2D979m9clfNnUYllF_wyRzzH-_NL45ebnONXGG_DGo7yVShAofFcQ9K373q01rUdjUj0bAz8P8iocQv5fY-dE30Xqudu1NJC19cN-U0rMTDVZKMJTfnGLZBJQWZ1pMuFCSLzL2uWiOdiu5OvS0P8pYGKqegV8nbm5Mu4F7yY6LOweLM8DoHyOZ3WGS-o6Dns_Xla37mACcssLJEhQnTOJX1bDlib9g";
    private String tokenSite = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ";

    @Test
    public void jwtTestPSB(){
        boolean isSigned = Jwts.parser().isSigned(tokenPSB);
        String[] splitToken = tokenPSB.split("\\.");
        Jwt<Header, Map> parsedToken = Jwts.parser()
                .setAllowedClockSkewSeconds(10531555)
                .parse(splitToken[0] + "." + splitToken[1] + ".");
        parsedToken.getBody();
        Assert.assertEquals("Смирнов Джон Абрамович", parsedToken.getBody().get("incbt/usrfull"));
    }

    @Test
    public void testFromSite(){
        Jwt<Header, Map> parsedToken = Jwts.parser().setSigningKey("secret".getBytes()).parse(tokenSite);
        parsedToken.getBody();
        Assert.assertEquals("John Doe", parsedToken.getBody().get("name"));
    }
}
