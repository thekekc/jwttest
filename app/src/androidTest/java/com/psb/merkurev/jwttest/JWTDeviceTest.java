package com.psb.merkurev.jwttest;

import android.support.test.runner.AndroidJUnit4;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.ByteArrayInputStream;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.util.Map;

import io.jsonwebtoken.Header;
import io.jsonwebtoken.Jwt;
import io.jsonwebtoken.Jwts;

/**
 * Created by merkurev on 14.11.16.
 *
 */

@RunWith(AndroidJUnit4.class)
public class JWTDeviceTest {
    private String tokenPSB = "eyJhbGciOiJSUzI1NiIsImtpZCI6IkNBMDA2MDcwNUYwQTBCOERDOTcxRURBNjQ2ODUyM0RDQTRGQzJEMDgiLCJ0eXAiOiJKV1QiLCJ4NXQiOiJ5Z0JnY0Y4S0M0M0pjZTJtUm9VajNLVDhMUWcifQ.eyJuYmYiOjE0NzkxMzQ2NjksImV4cCI6MTQ3OTEzNTI2OSwiaXNzIjoiaHR0cHM6Ly9yYnAtY2xpZW50YXBwL2lkZW50aXR5IiwiYXVkIjoiaHR0cHM6Ly9yYnAtY2xpZW50YXBwL2lkZW50aXR5L3Jlc291cmNlcyIsImNsaWVudF9pZCI6InJicENsaWVudE1vYmlsZSIsInN1YiI6IjkiLCJhdXRoX3RpbWUiOjE0NzkxMzQ2NjksImlkcCI6ImxvY2FsIiwiaW5jYnQvc2Vzc2lkIjoiNTE0IiwiaW5jYnQvYXBwaWQiOiIyIiwiaW5jYnQvY2hubGlkIjoiMiIsImluY2J0L3VzcmZ1bGwiOiLQodC80LjRgNC90L7QsiDQlNC20L7QvSDQkNCx0YDQsNC80L7QstC40YciLCJpbmNidC9wcm0iOlsiQWNjb3VudC5BY2NlcHQiLCJBY2NvdW50LlZpZXciXSwiaW5jYnQvY2xudGlkIjoiMTMiLCJzY29wZSI6WyJjbGllbnRhcGkiLCJpZGVudGl0eSIsIm9mZmxpbmVfYWNjZXNzIl0sImFtciI6WyJyYnBfc2VjcmV0Y29kZSJdfQ.SzQE5rJKYBsPbifTClrFG9T6JCs1oW77_sLfHfsm0dibUXH8ie0iG-5sgxygfYJhvmJepAPyd2CIWuvIyOrIhXpkn48X7lt6rF9QVpeFjAPOYPdMpuQDo2Uy2D979m9clfNnUYllF_wyRzzH-_NL45ebnONXGG_DGo7yVShAofFcQ9K373q01rUdjUj0bAz8P8iocQv5fY-dE30Xqudu1NJC19cN-U0rMTDVZKMJTfnGLZBJQWZ1pMuFCSLzL2uWiOdiu5OvS0P8pYGKqegV8nbm5Mu4F7yY6LOweLM8DoHyOZ3WGS-o6Dns_Xla37mACcssLJEhQnTOJX1bDlib9g";
    private String tokenSite = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ";
    private String keyPSB = "-----BEGIN CERTIFICATE-----\n" +
            "MIIDEDCCAfigAwIBAgIQS26d/8YpwIJNYBx0zoi9IzANBgkqhkiG9w0BAQsFADAU\n" +
            "MRIwEAYDVQQDEwlsb2NhbGhvc3QwHhcNMTYxMDA1MjEwMDAwWhcNMjExMDA1MjEw\n" +
            "MDAwWjAUMRIwEAYDVQQDEwlsb2NhbGhvc3QwggEiMA0GCSqGSIb3DQEBAQUAA4IB\n" +
            "DwAwggEKAoIBAQCk7Ni38aOIEdid99O83fgtkBrJE7LH1Hf4UJ/D3FsvvlsnmWHY\n" +
            "DuZEJrVJViaY0ugIeNzTnmmqb70SjiUBeLVZZaWHouclc47fgy0lEXdT5pfcw0SO\n" +
            "XTv44kqL6WmxQHo6BkglaDRMbHj1/gWVIEXcQaz8qW4M4uKSTWVMg37hIemzxQYF\n" +
            "8CZGRmaHgAv0e/gd4vEUKJqyo3brpTb5gGamtS904avJPezfdX/wF9yv5xwdRCa+\n" +
            "rTc31Lkw4Pwy6eCL3wwECLTvFHJiYbFTp3fu2RXtM9g8ikpNmdSBh7c8MKI0ukHC\n" +
            "WRhbSimoPDSYmj5m5nQS7hNECh/gXwNqL4LLAgMBAAGjXjBcMBMGA1UdJQQMMAoG\n" +
            "CCsGAQUFBwMDMEUGA1UdAQQ+MDyAEBppIDnuRM0rTDh6LELmkP2hFjAUMRIwEAYD\n" +
            "VQQDEwlsb2NhbGhvc3SCEEtunf/GKcCCTWAcdM6IvSMwDQYJKoZIhvcNAQELBQAD\n" +
            "ggEBAIbyUf362kaC6MTIb6jkCSR1bhr510aU0Bm4a4hpHXjK+d0uhWyuAFs7ySh5\n" +
            "mDHOIXIGYFGi/8juioWCJIzcciYSD0h7HgGBYuuIcfz1VT+1qg/w7Eg/tvLFMjH3\n" +
            "rF11MnhI1Mwjf4jQz8jHyBzL4K/4mUyN8N+G0+mzO0fgFKTRxvncQMHi3D3+2gj2\n" +
            "cts1MRIocNnWc8Y/a3nBN1gmSRLLVtMtRzXeTCXD45SWZunm0kNijg0aczkb9Idd\n" +
            "LSqKJ6QvAO5NCQZOy2n4cf09Lj+VxGqHNmxqz1cTWBLdgJW9AvT/X3gu18qUY+6s\n" +
            "Kr52KcepYy0A2FoyUjkUyW+CQ9M=\n" +
            "-----END CERTIFICATE----- ";

    @Test
    public void jwtTestPSB(){
        String[] splitToken = tokenPSB.split("\\.");
        Jwt<Header, Map> parsedToken = Jwts.parser()
                .setAllowedClockSkewSeconds(10531555)
                .parse(splitToken[0] + "." + splitToken[1] + ".");
        parsedToken.getBody();
        Assert.assertEquals("Смирнов Джон Абрамович", parsedToken.getBody().get("incbt/usrfull"));
    }

    @Test
    public void testFromSite(){
        Jwt<Header, Map> parsedToken = Jwts.parser().setSigningKey("secret".getBytes()).parse(tokenSite);
        parsedToken.getBody();
        Assert.assertEquals("John Doe", parsedToken.getBody().get("name"));
    }

    @Test
    public void testPSBWithKey() throws NoSuchAlgorithmException, InvalidKeySpecException, CertificateException {

        CertificateFactory factory = CertificateFactory.getInstance("X.509");
        X509Certificate  certificate = (X509Certificate) factory.generateCertificate(new ByteArrayInputStream(keyPSB.getBytes()));

        Jwt<Header, Map> parsedToken = Jwts.parser()
                .setSigningKey(certificate.getPublicKey())
                .setAllowedClockSkewSeconds(10531555)
                .parse(tokenPSB);
        parsedToken.getBody();
        Assert.assertEquals("Смирнов Джон Абрамович", parsedToken.getBody().get("incbt/usrfull"));
    }

}
