package com.psb.merkurev.jwttest;

import com.google.gson.Gson;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * Created by merkurev on 18.11.16.
 *
 */

public class GsonBigDecimalTest {
    private String json = "{\"value\": \"123.0\",\"name\": \"Hello World\"}";

    @Test
    public void testBigDecimal(){
        Gson gson = new Gson();
        Model model = gson.fromJson(json, Model.class);
        Assert.assertNotNull(model);
        Assert.assertEquals(model.getValue().doubleValue(), 123, 0.1);
    }

    public class Model {
        BigDecimal value;
        String name;

        public BigDecimal getValue() {
            return value;
        }

        public void setValue(BigDecimal value) {
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
